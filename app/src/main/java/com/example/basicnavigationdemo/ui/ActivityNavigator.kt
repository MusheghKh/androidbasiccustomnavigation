package com.example.basicnavigationdemo.ui

interface ActivityNavigator {

    fun navigate(dest: Navigator.FragmentDestination)

    fun popBackStack()

    fun showDialog(dest: Navigator.DialogDestination)

    fun onDialogCancel(tag: String?)

    fun cacnelDialog(tag: String)

}