package com.example.basicnavigationdemo.ui.tab1

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.basicnavigationdemo.ui.ActivityNavigator
import com.example.basicnavigationdemo.ui.Navigator
import com.example.basicnavigationdemo.R
import com.example.basicnavigationdemo.databinding.FragmentTab1Binding
import com.example.basicnavigationdemo.ui.nav1.Nav1Fragment

class Tab1Fragment : Fragment(R.layout.fragment_tab1) {

    companion object {
        fun newInstance(activityNavigator: ActivityNavigator) = Tab1Fragment().apply {
            navigator = activityNavigator
        }
    }

    private lateinit var navigator: ActivityNavigator
    private lateinit var binding: FragmentTab1Binding

    private val viewModel: Tab1ViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentTab1Binding.bind(view)

        binding.button.setOnClickListener {
            navigator.navigate(
                Navigator.FragmentDestination(
                    Nav1Fragment.newInstance(navigator),
                    "NAV!",
                    "nav1"
                ),
            )
        }

        viewModel.asd.observe(viewLifecycleOwner) {
            binding.text.text = it.toString()
        }
    }


}