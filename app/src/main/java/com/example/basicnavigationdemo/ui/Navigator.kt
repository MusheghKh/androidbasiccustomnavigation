package com.example.basicnavigationdemo.ui

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.example.basicnavigationdemo.R

class Navigator private constructor(
    private var activity: AppCompatActivity,
    @IdRes private val containerId: Int,
    initialRouteName: String,
    private val initialRoutesGenerator: MutableMap<String, () -> FragmentDestination>
) {

    companion object {
        private val storage = mutableMapOf<String, Navigator>()

        fun get(
            name: String,
            activity: AppCompatActivity,
            @IdRes containerId: Int,
            initialRouteName: String,
            initialRoutesGenerator: MutableMap<String, () -> FragmentDestination>
        ): Navigator {
            var inst = storage[name]
            if (inst == null) {
                inst = Navigator(activity, containerId, initialRouteName, initialRoutesGenerator)
                storage[name] = inst
            } else {
                inst.activity = activity
            }
            return inst
        }
    }

    var saveStateMode = SaveStateMode.SAVE_ALL

    private var currentRouteName: String
    private val routes = mutableMapOf<String, MutableList<FragmentDestination>>()

    private val dialogPool = mutableListOf<DialogDestination>()

    init {
        currentRouteName = initialRouteName
//        initialRoutesGenerator.invoke().forEach {
//            createRoute(it.key, it.value)
//        }
    }

    fun restoreRoute() {
        changeRoute(currentRouteName)
    }

    fun changeRoute(name: String) {
        currentRouteName = name
        var route = routes[name]
        if (route == null) {
            route = mutableListOf()
            route.add(initialRoutesGenerator[name]!!.invoke())
            routes[name] = route
        } else {
            when (saveStateMode) {
                SaveStateMode.SAVE_ONLY_BASE -> {
                    val base = route.first()
                    route.clear()
                    route.add(base)
                }
                SaveStateMode.SAVE_NONE -> {
                    route.clear()
                    route.add(initialRoutesGenerator[name]!!.invoke())
                }
                SaveStateMode.SAVE_ALL -> {
                    // do not nothing
                }
            }
        }
        val dest = route.last()
        activity.supportFragmentManager
            .beginTransaction()
            .replace(containerId, dest.fragment, dest.tag)
            .commitNow()
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(route.size != 1)
        activity.title = dest.title

        for (d in dialogPool) {
            d.dialog.show(dest.fragment.childFragmentManager, dest.tag)
        }
    }

    fun showDialog(dest: DialogDestination) {
        dialogPool.add(dest)
        dest.dialog.show(routes[currentRouteName]!!.last().fragment.childFragmentManager, dest.tag)
    }

    fun onDialogCancel(tag: String?) {
        dialogPool.removeIf {
            it.tag == tag
        }
    }

    fun cancelDialog(tag: String) {
        val dest = dialogPool.find {
            it.tag == tag
        }
        dest?.dialog?.dialog?.cancel()
    }

    fun popBackStack() {
        val route = routes[currentRouteName]!!
        if (route.size == 1) {
            activity.finish()
            return
        }
        route.removeLast()
        val dest = routes[currentRouteName]!!.last()
        activity.supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right)
            .replace(containerId, dest.fragment, dest.tag)
            .commit()
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(route.size != 1)
        activity.title = dest.title
    }

    fun pushDestination(dest: FragmentDestination) {
        val route = routes[currentRouteName]!!
        route.add(dest)
        activity.supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
            .replace(containerId, dest.fragment, dest.tag)
            .commit()
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(route.size != 1)
        activity.title = dest.title
    }

    class FragmentDestination(
        val fragment: Fragment,
        val title: String,
        val tag: String
    )

    class DialogDestination(
        val dialog: DialogFragment,
        val tag: String
    )

    enum class SaveStateMode {
        SAVE_ALL,
        SAVE_ONLY_BASE,
        SAVE_NONE;
    }

}