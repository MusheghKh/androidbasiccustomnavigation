package com.example.basicnavigationdemo.ui

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.basicnavigationdemo.R
import com.example.basicnavigationdemo.databinding.ActivityMainBinding
import com.example.basicnavigationdemo.databinding.LayoutTabBinding
import com.example.basicnavigationdemo.ui.tab1.Tab1Fragment
import com.example.basicnavigationdemo.ui.tab2.Tab2Fragment
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity(), ActivityNavigator {

    private lateinit var binding: ActivityMainBinding
    private lateinit var tab1Binding: LayoutTabBinding
    private lateinit var tab2Binding: LayoutTabBinding

    private val navigator = Navigator.get("main", this, R.id.container, "tab1", mutableMapOf(
        "tab1" to {
            Navigator.FragmentDestination(
                Tab1Fragment.newInstance(this@MainActivity),
                "TaB1",
                "tab1"
            )
        },
        "tab2" to {
            Navigator.FragmentDestination(
                Tab2Fragment.newInstance(this@MainActivity),
                "TaB2",
                "tab2"
            )
        }
    ))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        tab1Binding = LayoutTabBinding.inflate(layoutInflater)
        tab2Binding = LayoutTabBinding.inflate(layoutInflater)

        setSupportActionBar(binding.toolbar)

        navigator.restoreRoute()

        binding.layoutBottomTabs.setSelectedTabIndicator(null)
        binding.layoutBottomTabs.addTab(binding.layoutBottomTabs.newTab())
        binding.layoutBottomTabs.addTab(binding.layoutBottomTabs.newTab())

        binding.layoutBottomTabs.getTabAt(0)!!.customView = tab1Binding.root
        binding.layoutBottomTabs.getTabAt(1)!!.customView = tab2Binding.root

        tab1Binding.image.setImageResource(R.drawable.ic_baseline_3d_rotation_24)
        tab1Binding.label.text = "Tab1"

        tab2Binding.image.setImageResource(R.drawable.ic_baseline_4k_24)
        tab2Binding.label.text = "tab2"

        tab1Binding.image.setBackgroundColor(Color.RED)
        tab1Binding.label.setTextColor(Color.RED)
        tab2Binding.image.background = null
        tab2Binding.label.setTextColor(Color.GRAY)

        binding.layoutBottomTabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        tab1Binding.image.setBackgroundColor(Color.RED)
                        tab1Binding.label.setTextColor(Color.RED)
                        tab2Binding.image.background = null
                        tab2Binding.label.setTextColor(Color.GRAY)
                        navigator.changeRoute("tab1")
                    }
                    1 -> {
                        tab1Binding.image.background = null
                        tab1Binding.label.setTextColor(Color.GRAY)
                        tab2Binding.image.setBackgroundColor(Color.RED)
                        tab2Binding.label.setTextColor(Color.RED)
                        navigator.changeRoute("tab2")
                    }
                    else -> {
                        // do not nothing
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onBackPressed() {
        navigator.popBackStack()
    }

    override fun navigate(dest: Navigator.FragmentDestination) {
        navigator.pushDestination(dest)
    }

    override fun popBackStack() {
        navigator.popBackStack()
    }

    override fun showDialog(dest: Navigator.DialogDestination) {
        navigator.showDialog(dest)
    }

    override fun onDialogCancel(tag: String?) {
        navigator.onDialogCancel(tag)
    }

    override fun cacnelDialog(tag: String) {
        navigator.cancelDialog(tag)
    }
}