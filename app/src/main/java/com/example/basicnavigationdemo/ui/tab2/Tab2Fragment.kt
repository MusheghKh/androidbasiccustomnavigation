package com.example.basicnavigationdemo.ui.tab2

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.basicnavigationdemo.ui.ActivityNavigator
import com.example.basicnavigationdemo.R
import com.example.basicnavigationdemo.databinding.FragmentTab2Binding

class Tab2Fragment : Fragment(R.layout.fragment_tab2) {

    companion object {
        fun newInstance(activityNavigator: ActivityNavigator) = Tab2Fragment().apply {
            navigator = activityNavigator
        }
    }

    private lateinit var navigator: ActivityNavigator
    private lateinit var binding: FragmentTab2Binding

    private val viewModel: Tab2ViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentTab2Binding.bind(view)

        viewModel.asd.observe(viewLifecycleOwner) {
            binding.textView.text = it.toString()
        }
    }
}