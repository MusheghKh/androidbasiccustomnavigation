package com.example.basicnavigationdemo.ui.nav1

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.basicnavigationdemo.ui.ActivityNavigator
import com.example.basicnavigationdemo.ui.Navigator
import com.example.basicnavigationdemo.R
import com.example.basicnavigationdemo.databinding.FragmentNav1Binding
import com.example.basicnavigationdemo.ui.dialog.FmDialog
import kotlinx.coroutines.delay

class Nav1Fragment : Fragment(R.layout.fragment_nav1) {

    companion object {
        fun newInstance(activityNavigator: ActivityNavigator) = Nav1Fragment().apply {
            navigator = activityNavigator
        }
    }

    private val viewModel: Nav1ViewModel by viewModels()

    private lateinit var binding: FragmentNav1Binding

    private lateinit var navigator: ActivityNavigator

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentNav1Binding.bind(view)

        binding.button2.setOnClickListener {
            navigator.popBackStack()
        }

        binding.dialog.setOnClickListener {
            navigator.showDialog(
                Navigator.DialogDestination(
                    FmDialog.newInstance(navigator),
                    "fmdi"
                )
            )
            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                delay(5000)
                navigator.cacnelDialog("fmdi")
            }
        }

        viewModel.asd.observe(viewLifecycleOwner) {
            binding.text.text = it.toString()
        }
    }
}