package com.example.basicnavigationdemo.ui.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import com.example.basicnavigationdemo.ui.ActivityNavigator
import com.example.basicnavigationdemo.R
import com.example.basicnavigationdemo.databinding.DialogFmBinding

class FmDialog : DialogFragment(R.layout.dialog_fm) {

    companion object {
        fun newInstance(activityNavigator: ActivityNavigator) = FmDialog().apply {
            navigator = activityNavigator
        }
    }

    private lateinit var binding: DialogFmBinding
    private lateinit var navigator: ActivityNavigator

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DialogFmBinding.bind(view)
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        navigator.onDialogCancel(tag)
    }


}